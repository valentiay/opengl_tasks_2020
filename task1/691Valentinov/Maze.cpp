#include <cassert>
#include "Maze.hpp"

void
dfs(std::vector<std::vector<char>> &dfsStates, unsigned int y, unsigned int x, unsigned int ySize, unsigned int xSize) {
    std::vector<char> variants = {1, 2, 3, 4};
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(variants.begin(), variants.end(), g);
    for (char i: variants) {
        switch(i) {
            case 1:
                if (0 < y && dfsStates[y - 1][x] == 0) {
                    dfsStates[y - 1][x] = 1;
                    dfs(dfsStates, y - 1, x, ySize, xSize);
                }
                break;
            case 2:
                if (x < xSize - 1 && dfsStates[y][x + 1] == 0) {
                    dfsStates[y][x + 1] = 2;
                    dfs(dfsStates, y, x + 1, ySize, xSize);
                }
                break;
            case 3:
                if (y < ySize - 1 && dfsStates[y + 1][x] == 0) {
                    dfsStates[y + 1][x] = 3;
                    dfs(dfsStates, y + 1, x, ySize, xSize);
                }
                break;
            case 4:
                if (0 < x && dfsStates[y][x - 1] == 0) {
                    dfsStates[y][x - 1] = 4;
                    dfs(dfsStates, y, x - 1, ySize, xSize);
                }
                break;
            default:
                break;
        }
    }
}

Maze::Maze(unsigned int height, unsigned int width) : startXPosition(1.5), startYPosition(1.5) {
    assert(height % 2 == 1);
    assert(width % 2 == 1);
    unsigned int ySize = height / 2;
    unsigned int xSize = width / 2;
    auto dfsStates = std::vector<std::vector<char>>(ySize, std::vector<char>(xSize, 0));
    dfsStates[0][0] = 1;
    dfs(dfsStates, 0, 0, ySize, xSize);
    mazeCells = std::vector<std::vector<bool> >(height, std::vector<bool>(width, true));
    for (unsigned int i = 0; i < ySize; i++) {
        for (unsigned int j = 0; j < xSize; j++) {
            switch (dfsStates[i][j]) {
                case 1:
                    mazeCells[2 * i + 1][2 * j + 1] = false;
                    mazeCells[2 * i + 2][2 * j + 1] = false;
                    break;
                case 2:
                    mazeCells[2 * i + 1][2 * j + 1] = false;
                    mazeCells[2 * i + 1][2 * j] = false;
                    break;
                case 3:
                    mazeCells[2 * i + 1][2 * j + 1] = false;
                    mazeCells[2 * i][2 * j + 1] = false;
                    break;
                case 4:
                    mazeCells[2 * i + 1][2 * j + 1] = false;
                    mazeCells[2 * i + 1][2 * j + 2] = false;
                    break;
                default:
                    break;
            }
        }
    }
    mazeCells[0][1] = false;
    mazeCells[height - 1][width - 2] = false;
}


std::vector<bool> &Maze::operator[](int idx) {
    return mazeCells[idx];
}

const std::vector<bool> &Maze::operator[](int idx) const {
    return mazeCells[idx];
}

unsigned long Maze::getHeight() const {
    return mazeCells.size();
}

unsigned long Maze::getWidth() const {
    assert(getHeight() > 0);
    return mazeCells[0].size();
}

std::ostream &operator<<(std::ostream &os, const Maze &maze) {
    for (const auto &line: maze.mazeCells) {
        for (const auto &element: line) {
            if (element) {
                os << "#";
            } else {
                os << " ";
            }
        }
        os << std::endl;
    }
    return os;
}
