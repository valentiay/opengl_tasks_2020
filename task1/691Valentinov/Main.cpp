#include "Main.hpp"
#include "MazeCameraMover.hpp"
#include "MazeApplication.hpp"

int main() {
    Maze maze(15, 17);
    MazeApplication app(maze);
    app.start();

    return 0;
}
