#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <random>


class Maze {
public:
    float startXPosition, startYPosition;

    Maze(unsigned int height, unsigned int width);

    std::vector<bool> &operator[](int idx);

    const std::vector<bool> &operator[](int idx) const;

    unsigned long getHeight() const;

    unsigned long getWidth() const;

    friend std::ostream &operator<<(std::ostream &os, const Maze& maze);

private:
    std::vector<std::vector<bool> > mazeCells;
};
