#include "MazeCameraMover.hpp"
#include "MazeApplication.hpp"
#include <Application.hpp>
#include <ShaderProgram.hpp>
#include <Mesh.hpp>


MazeApplication::MazeApplication(Maze &maze)
        : Application(std::make_shared<FreeCameraMover>()),
          _maze(&maze),
          _inactiveCameraMover(std::make_shared<MazeCameraMover>(maze)) {}

MazeApplication::~MazeApplication() = default;

void MazeApplication::makeScene() {
    Application::makeScene();

    _mazeMesh = createMazeMesh(*_maze);
    _mazeMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.5f)));

    _shader = std::make_shared<ShaderProgram>(
            "691ValentinovData1/simple.vert",
            "691ValentinovData1/simple.frag"
    );
}

void MazeApplication::draw() {
    Application::draw();

    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    _shader->use();

    _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
    _shader->setMat4Uniform("modelMatrix", _mazeMesh->modelMatrix());
    _mazeMesh->draw();

}

void MazeApplication::handleKey(int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS) {
        if (key == GLFW_KEY_C) {
            _cameraMover.swap(_inactiveCameraMover);
        }
    }
    Application::handleKey(key, scancode, action, mods);
}

MeshPtr MazeApplication::createMazeMesh(Maze& maze){
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    for(int y = 0; y < maze.getHeight(); ++y ){
        for(int x = 0; x < maze.getWidth(); ++x){
            if( !maze[y][x]){
                vertices.emplace_back(glm::vec3(y, x - 1, -0.5f));
                vertices.emplace_back(glm::vec3(y + 1.f, x, -0.5f));
                vertices.emplace_back(glm::vec3(y + 1.f, x - 1, -0.5f));

                vertices.emplace_back(glm::vec3(y, x - 1, -0.5f));
                vertices.emplace_back(glm::vec3(y, x, -0.5f));
                vertices.emplace_back(glm::vec3(y + 1.f, x, -0.5f));

                normals.emplace_back(glm::vec3(0.0f, 0.0f, 1.0f));
                normals.emplace_back(glm::vec3(0.0f, 0.0f, 1.0f));
                normals.emplace_back(glm::vec3(0.0f, 0.0f, 1.0f));

                normals.emplace_back(glm::vec3(0.0f, 0.0f, 1.0f));
                normals.emplace_back(glm::vec3(0.0f, 0.0f, 1.0f));
                normals.emplace_back(glm::vec3(0.0f, 0.0f, 1.0f));

                vertices.emplace_back(glm::vec3(y, x - 1, 0.5f));
                vertices.emplace_back(glm::vec3(y + 1.f, x, 0.5f));
                vertices.emplace_back(glm::vec3(y + 1.f, x - 1, 0.5f));

                vertices.emplace_back(glm::vec3(y, x - 1, 0.5f));
                vertices.emplace_back(glm::vec3(y, x, 0.5f));
                vertices.emplace_back(glm::vec3(y + 1, x, 0.5f));

                normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));
                normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));
                normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));

                normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));
                normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));
                normals.emplace_back(glm::vec3(0.0f, 0.0f, -1.0f));;
            }
            if(maze[y][x] && y > 0 && !maze[y - 1][x]){
                vertices.emplace_back(glm::vec3(y, x - 1, -0.5f));
                vertices.emplace_back(glm::vec3(y, x, 0.5f));
                vertices.emplace_back(glm::vec3(y, x - 1, 0.5f));

                vertices.emplace_back(glm::vec3(y, x - 1, -0.5f));
                vertices.emplace_back(glm::vec3(y, x, -0.5f));
                vertices.emplace_back(glm::vec3(y, x, 0.5f));

                normals.emplace_back(glm::vec3(0.0f, 1.f, 0.0f));
                normals.emplace_back(glm::vec3(0.0f, 1.f, 0.0f));
                normals.emplace_back(glm::vec3(0.0f, 1.f, 0.0f));

                normals.emplace_back(glm::vec3(0.0f, 1.f, 0.0f));
                normals.emplace_back(glm::vec3(0.0f, 1.f, 0.0f));
                normals.emplace_back(glm::vec3(0.0f, 1.f, 0.0f));

            }
            if(maze[y][x] && y + 1 < maze.getHeight() && !maze[y + 1][x]){
                vertices.emplace_back(glm::vec3(y + 1, x - 1, -0.5f));
                vertices.emplace_back(glm::vec3(y + 1, x, 0.5f));
                vertices.emplace_back(glm::vec3(y + 1, x - 1, 0.5f));

                vertices.emplace_back(glm::vec3(y + 1, x - 1, -0.5f));
                vertices.emplace_back(glm::vec3(y + 1, x, -0.5f));
                vertices.emplace_back(glm::vec3(y + 1, x, 0.5f));

                normals.emplace_back(glm::vec3(0.0f, -1.f, 0.0f));
                normals.emplace_back(glm::vec3(0.0f, -1.f, 0.0f));
                normals.emplace_back(glm::vec3(0.0f, -1.f, 0.0f));

                normals.emplace_back(glm::vec3(0.0f, -1.f, 0.0f));
                normals.emplace_back(glm::vec3(0.0f, -1.f, 0.0f));
                normals.emplace_back(glm::vec3(0.0f, -1.f, 0.0f));
            }
            if(maze[y][x] && x + 1 < maze.getWidth() && !maze[y][x + 1]){
                vertices.emplace_back(glm::vec3(y, x, -0.5f));
                vertices.emplace_back(glm::vec3(y + 1, x, 0.5f));
                vertices.emplace_back(glm::vec3(y, x, 0.5f));

                vertices.emplace_back(glm::vec3(y, x, -0.5f));
                vertices.emplace_back(glm::vec3(y + 1, x, -0.5f));
                vertices.emplace_back(glm::vec3(y + 1, x, 0.5f));

                normals.emplace_back(glm::vec3(1.0f, 0.f, 0.0f));
                normals.emplace_back(glm::vec3(1.0f, 0.f, 0.0f));
                normals.emplace_back(glm::vec3(1.0f, 0.f, 0.0f));

                normals.emplace_back(glm::vec3(1.0f, 0.f, 0.0f));
                normals.emplace_back(glm::vec3(1.0f, 0.f, 0.0f));
                normals.emplace_back(glm::vec3(1.0f, 0.f, 0.0f));
            }
            if(maze[y][x] && x > 0 && maze.getWidth() && !maze[y][x - 1]){
                vertices.emplace_back(glm::vec3(y, x - 1, -0.5f));
                vertices.emplace_back(glm::vec3(y + 1, x - 1, 0.5f));
                vertices.emplace_back(glm::vec3(y, x - 1, 0.5f));

                vertices.emplace_back(glm::vec3(y, x - 1, -0.5f));
                vertices.emplace_back(glm::vec3(y + 1, x - 1, -0.5f));
                vertices.emplace_back(glm::vec3(y + 1, x - 1, 0.5f));

                normals.emplace_back(glm::vec3(-1.0f, 0.f, 0.0f));
                normals.emplace_back(glm::vec3(-1.0f, 0.f, 0.0f));
                normals.emplace_back(glm::vec3(-1.0f, 0.f, 0.0f));

                normals.emplace_back(glm::vec3(-1.0f, 0.f, 0.0f));
                normals.emplace_back(glm::vec3(-1.0f, 0.f, 0.0f));
                normals.emplace_back(glm::vec3(-1.0f, 0.f, 0.0f));
            }

        }
    }
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());



    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());
    return mesh;
}