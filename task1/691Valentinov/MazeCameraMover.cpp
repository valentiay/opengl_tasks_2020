#include "MazeCameraMover.hpp"

#include <algorithm>

MazeCameraMover::MazeCameraMover(Maze &maze) :
        CameraMover(),
        maze(maze),
        _position(maze.startXPosition, maze.startYPosition, 0.5f),
        _rotation(glm::toQuat(glm::lookAt(_position, glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f)))) {}

void MazeCameraMover::handleKey(GLFWwindow *window, int key, int scancode, int action, int mods) {
}

void MazeCameraMover::handleMouseMove(GLFWwindow *window, double xpos, double ypos) {
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS) {
        double dx = xpos - PreviousXPosition;
        double dy = ypos - PreviousYPosition;

        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rotation;
        _rotation *= glm::angleAxis(static_cast<float>(dy * 0.002), rightDir);

        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rotation *= glm::angleAxis(static_cast<float>(dx * 0.002), upDir);
    }

    PreviousXPosition = xpos;
    PreviousYPosition = ypos;
}

void MazeCameraMover::handleScroll(GLFWwindow *window, double xoffset, double yoffset) {
}

void MazeCameraMover::update(GLFWwindow *window, double dt) {
    float speed = 1.2f;

    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rotation;

    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rotation;

    glm::vec3 delta(0, 0, 0);

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        delta += forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        delta -= forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        delta -= rightDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        delta += rightDir * speed * static_cast<float>(dt);
    }
    if ((delta[0] > 0 &&
         ((_position[0] + std::max(0.2f, delta[0])) > maze.getHeight() - 1 ||
          maze[_position[0] + std::max(0.2f, delta[0])][_position[1]])) ||
        (delta[0] < 0 && ((_position[0] + std::min(-0.2f, delta[0])) < 0 ||
                          maze[_position[0] + std::min(-0.2f, delta[0])][_position[1]]))) {
        delta[0] = 0;
    }
    if ((delta[1] > 0 &&
         ((_position[1] + std::max(0.2f, delta[1])) > maze.getWidth() - 1 ||
          maze[_position[0]][_position[1] + std::max(0.2f, delta[1])])) ||
        (delta[1] < 0 && ((_position[1] + std::min(-0.2f, delta[1])) < 0 ||
                          maze[_position[0]][_position[1] + std::min(-0.2f, delta[1])]))) {
        delta[1] = 0;
    }
    delta[2] = 0;

    _position += delta;
    _camera.viewMatrix = glm::toMat4(-_rotation) * glm::translate(-_position);
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float) width / height, 0.1f, 100.f);
}

void MazeCameraMover::move(glm::vec3 position) {
    _position = position;
}
