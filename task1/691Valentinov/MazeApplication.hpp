#pragma once

#include "MazeCameraMover.hpp"

#include <Application.hpp>
#include <ShaderProgram.hpp>
#include <glm/ext.hpp>
#include <Mesh.hpp>

#include <iostream>
#include <fstream>
#include <vector>

class MazeApplication : public Application {
protected:
    MeshPtr _mazeMesh;
    Maze *_maze;
    ShaderProgramPtr _shader;
    std::shared_ptr<CameraMover> _inactiveCameraMover;

public:
    explicit MazeApplication(Maze &maze);

    ~MazeApplication();

    void makeScene() override;

    void draw() override;

    void handleKey(int key, int scancode, int action, int mods) override;

    MeshPtr createMazeMesh(Maze& maze);
};