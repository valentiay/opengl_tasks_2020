#pragma once

#include <vector>
#include <iostream>

#include <Camera.hpp>
#include <glm/ext.hpp>
#include "Maze.hpp"

class MazeCameraMover : public CameraMover {
public:
    explicit MazeCameraMover(Maze &maze);

    void handleKey(GLFWwindow *window, int key, int scancode, int action, int mods) override;

    void handleMouseMove(GLFWwindow *window, double xpos, double ypos) override;

    void handleScroll(GLFWwindow *window, double xoffset, double yoffset) override;

    void update(GLFWwindow *window, double dt) override;

    void move(glm::vec3 position);

protected:
    glm::vec3 _position;
    glm::quat _rotation;

    double PreviousXPosition = 0.0;
    double PreviousYPosition = 0.0;
private:
    Maze &maze;
};
